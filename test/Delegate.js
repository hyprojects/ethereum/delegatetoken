const { BN, constants, expectEvent, expectRevert } = require('openzeppelin-test-helpers');
const EthUtil = require('ethereumjs-util');
const { expect } = require('chai');

var Delegate = artifacts.require("./Delegate.sol");

contract('Delegate', accounts => {
    const initialHolder = accounts[0]
    const recipient = accounts[1]
    const spender = accounts[2]

    let delegate = null

    beforeEach(async function() {
        delegate = await Delegate.new()
	})

    describe("Sign and Verify", function() {
        it("signs an approval", async function() {
            var message = web3.utils.soliditySha3(
                {
                    type: 'uint256',
                    value: '1000'
                }
            )

            // using web3, create a signature. Not available with Ganache so
            // create using web browser and web3 v1+ then assign hard-coded
            // signature to var.
            //var signature = await web3.eth.personal.sign(message, accounts[0]);
            // uncomment and add signature generated using peronal_sign:
             // var signature = ""

            var messageHash = web3.eth.accounts.hashMessage(message)

            const { logs } = await delegate.approveBySignature(
                initialHolder,
                spender,
                1000,
                signature)

            expectEvent.inLogs(
                logs,
                'Approval',
                {
                    owner: initialHolder,
                    spender: spender,
                    value: new BN(1000),
                }
            )

            allowance = await delegate.allowance(initialHolder, spender)

            expect(allowance).to.be.bignumber.equal("1000")
        })
    })
})
