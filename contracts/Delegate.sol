pragma solidity >=0.4.21 <0.6.0;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
import "openzeppelin-solidity/contracts/cryptography/ECDSA.sol";

contract Delegate is ERC20, ERC20Detailed {
    using ECDSA for bytes32;

    event Hashed(bytes32 hash);

    constructor()
        ERC20Detailed("Delegate", "DLGT", 4)
        public {
        uint256 initialBalance = 1000000 * (10 ** uint256(decimals()));
        _mint(msg.sender, initialBalance);
    }

    function approveBySignature(
        address owner,
        address spender,
        uint256 amount,
        bytes memory signature) public returns (bool) {
        bytes32 hash = keccak256(abi.encodePacked(amount)).toEthSignedMessageHash();
        if (owner != hash.recover(signature)) {
            revert('The owner did not sign this approval.');
        }

        _approve(owner, spender, amount);
        return true;
    }
}
